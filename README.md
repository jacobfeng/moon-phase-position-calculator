# README #

Calculator for moon's phase, elongation, position in ecliptic/equatorial system.

Compile:

make

Run:

./calc --year [2019] --month [12] --date [27] --UT [0] --lat [39.9042] --lon [116.4074]

Feel free to use the code, algorithm is based on Paul Schlyter's webpage: http://www.stjarnhimlen.se/comp/tutorial.html
