// calculator for moon's position, elongation, phase angle

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <getopt.h>
#include <math.h>

#define PI 3.14159265358979323846
#define RADEG (180.0/PI)
#define DEGRAD (PI/180.0)
#define sind(x) sin((x)*DEGRAD)
#define cosd(x) cos((x)*DEGRAD)
#define tand(x) tan((x)*DEGRAD)
#define asind(x) (RADEG*asin(x))
#define acosd(x) (RADEG*acos(x))
#define atand(x) (RADEG*atan(x))
#define atan2d(y,x) (RADEG*atan2((y),(x)))

#define MERCURY 0
#define VENUS 1
#define MARS 2
#define JUPITER 3
#define SATURN 4
#define URANUS 5
#define NEPTUNE 6

#define MERCURYAPPRENTDIA 6.74
#define VENUSAPPRENTDIA 6.74
#define MERCURYAPPRENTDIA 6.74


double rev( double x )
{
	return x - floor(x/360.0)*360.0;
}


double cbrt( double x )
{
	if ( x > 0.0 )
		return exp( log(x) / 3.0 );
	else if ( x < 0.0 )
		return -cbrt(-x);
	else /* x == 0.0 */
		return 0.0;
}

void sph2xyz(double * radecIn, double * xyzOut){
	//input: radecIn, an array like [RA, DEC, r]
	//output: xyzOut, an array like [x, y, z]
	xyzOut[0] = radecIn[2] * cosd(radecIn[0]) * cosd(radecIn[1]);
	xyzOut[1] = radecIn[2] * sind(radecIn[0]) * cosd(radecIn[1]);
	xyzOut[2] = radecIn[2] * sind(radecIn[1]);
	return;
}

void xyz2sph(double * xyzIn, double * radecOut){
	radecOut[2] = sqrt(xyzIn[0]*xyzIn[0] + xyzIn[1]*xyzIn[1] + xyzIn[2]*xyzIn[2]);
	radecOut[0] = rev(atan2d(xyzIn[1], xyzIn[0]));
	radecOut[1] = atan2d(xyzIn[2], sqrt(xyzIn[0]*xyzIn[0]+ xyzIn[1]*xyzIn[1]));
	return;
}

void eclip2equat(double * eclipIn, double * equatOut, double obliquity){
	//obliquity is defined as in degree, approx. 23.4
	equatOut[0] = eclipIn[0];
	equatOut[1] = eclipIn[1] * cosd(obliquity) - eclipIn[2] * sind(obliquity);
	equatOut[2] = eclipIn[1] * sind(obliquity) + eclipIn[2] * cosd(obliquity);
	return;
}

void equat2eclip(double * equatIn, double * eclipOut, double obliquity){
	//obliquity is defined as in degree, approx. 23.4
	eclipOut[0] = equatIn[0];
	eclipOut[1] = equatIn[1] * cosd(-1*obliquity) - equatIn[2] * sind(-1*obliquity);
	eclipOut[2] = equatIn[1] * sind(-1*obliquity) + equatIn[2] * cosd(-1*obliquity);
	return;
}

double daysfromJD(int year, int month, int date, double ut){
	//formula valid from 1900 to 2100
	int days = 0;
	days = 367*year - (7*(year+((month+9)/12)))/4 + (275*month)/9 + date - 730530;
	return (double)days+ut/24;
}


double sunMeanLogitude(double d){
	double omega = rev(282.9404 + 4.70935e-5 * d); //longitude of perihelion in degree
	double M = rev(356.0470 + 0.985600285 * d); //mean anomaly
	double L = rev(omega + M);
	return L;
}

//calculate sidereal time
double siderealTime(double d, double UT, double LON){
	//here we assume d doesn't account UT time, so is 00:00 right now
	//d is day from julian date, use daysfromJD to calculate, UT is the ut time in decimal, LON is longitude in degree
	//output is in hour
	double result = 0;
	double GMST0 = (rev(sunMeanLogitude(d)+180))/15; //in hour, sidereal time at greenwich meridian at 00:00 right now
	result = GMST0 + UT + LON/15;
	if(result<0){
		result = result+24;
	}
	return result;
}

//calculate hour angle
double hourAngel(double sidTime, double RAhour){
	// sidTime and RA must be in the same unit here we assume RA is in unit of hour
	return sidTime - RAhour;
}


void radec2azialt(double LAT, double HA, double * radec, double * aziAlt){
	double xyz[3];
	xyz[0] = cosd(HA) * cosd(radec[1]);
	xyz[1] = sind(HA) * cosd(radec[1]);
	xyz[2] = sind(radec[1]);
	double xyzhor[3];
	xyzhor[0] = xyz[0] * cosd(90-LAT) - xyz[2]*sind(90-LAT);
	xyzhor[1] = xyz[1];
	xyzhor[2] = xyz[0] * sind(90-LAT) + xyz[2]*cosd(90-LAT);

	aziAlt[0] = atan2d(xyzhor[1], xyzhor[0]) + 180; //azimuth
	aziAlt[1] = atan2d(xyzhor[2], sqrt(xyzhor[0]*xyzhor[0] + xyzhor[1]*xyzhor[1])); //altitutde
	return;
}

// calculate sun's position in ra and dec
void sunRADEC(double d, double UT, double LAT, double LON, double * radec, double * aziAlt){
	//calculate sun's position for date,
	//input: date (number of days from JD, use daysfromJD function to calculate, not account UT, so 00:00 ut time right now) and an empty array
	//input, UT time in decimal, LON, longitude in degree
	//output: radec array will be filled with, RA, DEC (in degree), and radius, aziAlt is a 2d array, filled with azimuth angle and altitude (in degree)
	double omega = rev(282.9404 + 4.70935e-5 * d); //longitude of perihelion in degree
	double e = 0.016709 - 1.151e-9 * d; //eccentricity
	double M = rev(356.0470 + 0.985600285 * d); //mean anomaly
	double E = rev(M + (180/PI) * e * sind(M) * (1 + e*cosd(M))); //auxilliarry angle
	double x = cosd(E) - e; //calculate sun's rectangular coor in ecliptic plane
	double y = sind(E) * sqrt(1-e*e);
	double r = sqrt(x*x + y*y); //distance and true anomaly
	double v = atan2d(y, x);
	double lon = rev(v + omega);
	double recEclipCoo[3];
	recEclipCoo[0] = r * cosd(lon); //calculate rectangular coordinates, rotate to equatorial coordinates and compute RA and DEC
	recEclipCoo[1] = r * sind(lon);
	recEclipCoo[2] = 0;
	double recEquatCoo[3];
	eclip2equat(recEclipCoo, recEquatCoo, 23.4406); //rotate to equatorial coordinates
	xyz2sph(recEquatCoo, radec);
	double sidTime = siderealTime(d, UT, LON);
	double hrangle = rev(hourAngel(sidTime, radec[0]/15) * 15); //hour angle in degree
	//coordinate system transformation
	radec2azialt(LAT, hrangle, radec, aziAlt); //convert to altitude and azimuth
	return;
}

//calculate moon's position in ra and dec
void moonRADEC(double d, double UT, double LAT, double LON, unsigned int parallaxCorrect, double * radec, double * aziAlt, double * auxinfo){
	//given days from JD, UT time, latitutde, longitude, return ra dec and altitude azimuth, parralax is corrected if parallaxCorrect is 1
	//output: returns the topocentric cooridnate of moon now
	double N = rev(125.1228 - 0.0529538083*d); //in degree, longitudinal ascending node
	double i = 5.1454; //inclination of the orbit
	double omega = rev(318.0634 + 0.1643573223*d); //in degree, argument of perigee
	double a = 60.2666; //mean distance, in unit of ???
	double e = 0.054900; //eccentricity
	double M = rev(115.3654 + 13.0649929509*d); //mean anomaly

	double eTolerance = 0.005; //tolerances for approximation of eccentricity of the moon's orbit. E will be updated with each iteration
	double E0 = rev(M + (180/PI) * e * sind(M) * (1 + e * cosd(M)));
	double E1 = 0;
	while(fabs(E1 - E0)>eTolerance){
		E0 = E1;
		E1 = rev(E0 - (E0 - (180/PI) * e * sind(E0) - M) / (1 - e * cosd(E0)));
	}
	double E = E1;

	//calculate moon's position in plane of the lunar orbit, xy coordinate
	double xy[2];
	xy[0] = a * (cosd(E) - e);
	xy[1] = a * sqrt(1 - e*e) * sind(E);
	//r v coordinate
	double rv[2];
	rv[0] = sqrt(xy[0]*xy[0] + xy[1]*xy[1]);
	rv[1] = rev(atan2d(xy[1],xy[0]));

	double xyzEclip[3];
	xyzEclip[0] = rv[0] * (cosd(N) * cosd(rv[1]+omega) - sind(N) * sind(rv[1]+omega) * cosd(i));
	xyzEclip[1] = rv[0] * (sind(N) * cosd(rv[1]+omega) + cosd(N) * sind(rv[1]+omega) * cosd(i));
	xyzEclip[2] = rv[0] * sind(rv[1] + omega) * sind(i);

	double lonLatDis[3]; // longitude, latitutde, distance
	lonLatDis[0] = rev(atan2d(xyzEclip[1], xyzEclip[0]));
	lonLatDis[1] = (atan2d(xyzEclip[2], sqrt(xyzEclip[0] * xyzEclip[0] + xyzEclip[1] * xyzEclip[1])));
	lonLatDis[2] = sqrt(xyzEclip[0] * xyzEclip[0] + xyzEclip[1] * xyzEclip[1] + xyzEclip[2] * xyzEclip[2]);
	
	double lonSun = sunMeanLogitude(d);
	double lonMoon = rev(N + omega + M);
	double Msun = rev(356.0470 + 0.985600285 * d); 
	double D = lonMoon - lonSun; //mean elongation
	double F = lonMoon - N; //argument of latitutde

	//21 terms of perturbations
	double perturbationLon[12];
	double perturbationLat[5];
	double perturbationDist[3];

	perturbationLon[0] = -1.274 * sind(M - 2 * D);
	perturbationLon[1] =  0.658 * sind(2 * D);
	perturbationLon[2] = -0.186 * sind(Msun);
	perturbationLon[3] = -0.059 * sind(2 * M - 2 * D);
	perturbationLon[4] = -0.057 * sind(M - 2 * D + Msun);
	perturbationLon[5] =  0.053 * sind(M + 2 * D);
	perturbationLon[6] =  0.046 * sind(2 * D - Msun);
	perturbationLon[7] =  0.041 * sind(M - Msun);
	perturbationLon[8] = -0.035 * sind(D);
	perturbationLon[9] = -0.031 * sind(M + Msun);
	perturbationLon[10]= -0.015 * sind(2 * F - 2 * D);
	perturbationLon[11]=  0.011 * sind(M - 4 * D);

	double perturbationLonTotal = 0;
	for(int i=0;i<12;i++){
		perturbationLonTotal += perturbationLon[i];
	}

	perturbationLat[0] = -0.173 * sind(F - 2 * D);
	perturbationLat[1] = -0.055 * sind(M - F - 2 * D);
	perturbationLat[2] = -0.046 * sind(M + F - 2 * D);
	perturbationLat[3] =  0.033 * sind(F + 2 * D);
	perturbationLat[4] =  0.017 * sind(2 * M + F);

	double perturbationLatTotal = 0;
	for(int i=0;i<5;i++){
		perturbationLatTotal += perturbationLat[i];
	}

	perturbationDist[0] = -0.58 * cosd(M - 2 * D);
	perturbationDist[1] = -0.46 * cosd(2 * D);

	double perturbationDistTotal = 0;
	for(int i=0;i<2;i++){
		perturbationDistTotal += perturbationDist[i];
	}

	lonLatDis[0] += perturbationLonTotal;
	lonLatDis[1] += perturbationLatTotal;
	lonLatDis[2] += perturbationDistTotal;

	double oblecl = 23.4393 - 3.563e-7 * d;
	double xeclip = lonLatDis[2] * cosd(lonLatDis[0]) * cosd(lonLatDis[1]);
	double yeclip = lonLatDis[2] * sind(lonLatDis[0]) * cosd(lonLatDis[1]);
	double zeclip = lonLatDis[2] * sind(lonLatDis[1]);

	double xequat = xeclip;
	double yequat = yeclip * cosd(oblecl) - zeclip * sind(oblecl);
	double zequat = yeclip * sind(oblecl) + zeclip * cosd(oblecl);

	radec[0] = rev(atan2d(yequat, xequat));
	radec[1] = atan2d(zequat, sqrt(xequat*xequat + yequat*yequat));

	if(parallaxCorrect==0){
		double LST = siderealTime(d, UT, LON);
		double HA = rev(LST * 15 - radec[0]);
		radec2azialt(LAT, HA, radec, aziAlt);
		return;
	}
		
	//parallax correction
	double gcLAT = LAT - 0.1924 * sind(2 * LAT);
	double rho = 0.99833 + 0.00167 * cosd(2 * LAT);

	double LST = siderealTime(d, UT, LON);
	double HA = rev(LST * 15 - radec[0]);

	double mpar = asind(1/lonLatDis[2]);

	double g = atand(tand(gcLAT)/cosd(HA)); //auxillary angle
	double topRA = rev(radec[0] - mpar * rho * cosd(gcLAT) * sind(HA) / cosd(radec[1]));
	double topDEC = radec[1] - mpar * rho * sind(gcLAT) * sind(g - radec[1]) / sind(g);

	radec[0] = topRA;
	radec[1] = topDEC;

	radec2azialt(gcLAT, HA, radec, aziAlt);

	//calculate distance, planet, sun, earth, elongation, phase angle, phase value
	double slon = lonSun;
	double mlon = lonLatDis[0];
	double mlat = lonLatDis[1];

	double elong = acosd(cosd(slon - mlon) * cosd(mlat));
	double FV = 180 - elong;
	double phase = (1+cosd(FV))/2;

	auxinfo[0] = elong;
	auxinfo[1] = FV;
	auxinfo[2] = phase;

	return;
}


int main(int argc, char ** argv){

	int year = 2019;
	int month = 12;
	int date = 27;
	double UT = 0;

	double lat = 39.9042;
	double lon = 116.4074;
	unsigned int correctParallaxFlag = 1;

	double radecMoonCoo[2];
	double azalMoonCoo[2];
	double auxInfoMoon[3];

	static struct option long_options[] = {
		{"year", required_argument, NULL, 'y'},
		{"month", required_argument, NULL, 'm'},
		{"date", required_argument, NULL, 'd'},
		{"ut", required_argument, NULL, 't'},
		{"lat", required_argument, NULL, 'a'},
		{"lon", required_argument, NULL, 'b'},
		{"help", no_argument, NULL, 'h'}
	};

	char * optstring = "a::b::c::d::e::f::g";;
	int opt;
	int option_index = 0;

	while((opt = getopt_long_only(argc, argv, optstring, long_options, &option_index))!=-1){
		switch(opt){
			case 'y':
				year = atoi(optarg);
			break;
			case 'm':
				month = atoi(optarg);
			break;
			case 'd':
				date = atoi(optarg);
			break;
			case 't':
				UT = atof(optarg);			
			break;
			case 'a':
				lat = atof(optarg);			
			break;
			case 'b':
				lon = atof(optarg);			
			break;
			case 'h':
				printf("USAGE: ./calc --year [2019] --month [12] --date [27] --UT [0] --lat [39.9042] --lon [116.4074]");
			return 0;
		}
	}


	printf("yyyy-mm-dd: %04d-%02d-%02d, UT: %f, @Lat: %03.3f (deg), Lon:%03.3f (deg)\n", year, month, date, UT, lat, lon);
	moonRADEC(daysfromJD(year, month, date, UT), UT, lat, lon, correctParallaxFlag, radecMoonCoo, azalMoonCoo, auxInfoMoon);
	printf("Moon: RA: %f deg, DEC: %f deg, Azi (0 N-E-S-W 360): %f deg, Alt (0 ground-zenith 90): %f deg\n", radecMoonCoo[0], radecMoonCoo[1], azalMoonCoo[0], azalMoonCoo[1]);
	printf("\telong: %f deg, FV (full 0 - 180 new): %f deg, phase (new 0-1 full): %f \n", auxInfoMoon[0], auxInfoMoon[1], auxInfoMoon[2]);

	return 0;
}